# The FrogandOwl.org Web Site

## Branches

* The **master** branch contains the site as we develop it.
* The **ticgit** branch contains our issue queue, see below for more information.

## Issues

We use [ticgit][ticgit] to track issues for the web site.  Ticgit is an easy-to-use command line tool that keeps all 
the issue queue data right in the repo, so there is only ever one thing to update.

[ticgit]: http://github.com/schacon/ticgit "ticgit ticket tracking system"

### Usage notes:

* "man ti" will give you helpful usage information.  Most commands can also be given the -h option to access help for 
that command.
* By default, ticgit assigns a new ticket to whomever created it.  Please remember to "ti assign -u nobody \<ticket\>" 
if it shouldn't be assigned to you.
* If you want to be in charge of a task, please do "ti assign -u \<ticket\>" (omitting a user name assigns it to 
yourself).
* Let others know what ticket you are currently working on by checking that ticket out with "ti checkout \<ticket\>" or 
using the "-c" option when you assign it.
* Make sure to comment on issues as you go so we can keep track of our progress, and you can easily pick up where you 
left off when you take a break.

## To do:

* Best practices section.
* List of most important docs.
* URI for demo site.
* Info where we are using custom code (nowhere at the moment).
* Other caveats/gotchas.
